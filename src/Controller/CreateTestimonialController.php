<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Testimonial;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\TestimonialFormType;

class CreateTestimonialController extends AbstractController
{
    #[Route('/testimonial/create', name: 'app_create_testimonial')]
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
        $testimonial = new Testimonial();
        $form = $this->createForm(TestimonialFormType::class, $testimonial);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            // On récupère la photo transmise
            $photo = $form->get('photo')->getData();

            $fichier = md5(uniqid()). '.'.$photo->guessExtension();

            $photo->move(
                $this->getParameter("images_directory"),
                $fichier
            );

            $testimonial->setPhoto('../img/'.$fichier);
            $testimonial->setUser($this->getUser());

            $entityManager->persist($testimonial);
            $entityManager->flush();

            return $this->redirectToRoute('app_home');
        }
        return $this->render('create_testimonial/index.html.twig', [
            'formRender' => $form->createView(),
        ]);
    }
}
