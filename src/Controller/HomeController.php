<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Testimonial;


class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(ManagerRegistry $doctrine, EntityManagerInterface $em): Response
    {
        $dql = "SELECT a FROM App:Testimonial a WHERE a.stars > :stars ORDER BY a.id DESC";       
        $query = $em->createQuery($dql)->setParameter('stars', 2)->setMaxResults(3);
        $testimonial = $query->getResult();

        return $this->render('home/index.html.twig', [
            'testimonials' => $testimonial,
        ]);
    }
}
